/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.*;

/**
 *
 * @author Roberto
 */
@Stateless
public class Libro2Bean implements Libro2BeanRemote {
    @PersistenceContext(name ="LIBRERIA2-ejbPU")
    EntityManager em;
    protected Libro libro;
    protected Collection <Libro> listalibros;
    
    @Override
    public void addLibro(String autor, String titulo, BigDecimal precio) {
        if(libro == null)
            {
            libro=new Libro (titulo, autor, precio);
            em.persist(libro);
            libro =null;
            }
    }


    @Override
    public void updateLibro(Integer id, String autor, String titulo, BigDecimal precio) {
        libro=findLibro(id);
        if(libro!=null){
            libro.setTitulo(titulo);
            libro.setAutor(autor);
            libro.setPrecio(precio);
            em.merge(libro);
            libro = null;    
        }
    }

    @Override
    public void deleteLibro(Integer id) {
        libro=findLibro(id);
        em.remove(libro);
        libro=null;
    }

    @Override
    public Libro findLibro(Integer id) {
        libro=em.find(Libro.class, id);
        return libro;
    }

    @Override
    public Collection<Libro> getAllLibros() {
      listalibros = em.createNamedQuery("Libro.findAll").getResultList();
      return listalibros;
    }
}
