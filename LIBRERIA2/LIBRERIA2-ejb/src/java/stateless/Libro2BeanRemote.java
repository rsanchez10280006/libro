/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import javax.ejb.Remote;
import entidad.*;
import java.math.BigDecimal;
import java.util.Collection;
/**
 *
 * @author Roberto
 */
@Remote
public interface Libro2BeanRemote {
    public void addLibro(String autor, String titulo, BigDecimal precio);
    public Collection <Libro> getAllLibros();
    public Libro findLibro(Integer id);
    public void updateLibro(Integer id, String autor, String titulo, BigDecimal precio);
    public void deleteLibro(Integer id);
}
