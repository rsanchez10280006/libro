<%-- 
    Document   : eliminar
    Created on : 7/11/2014, 10:56:45 AM
    Author     : Roberto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entidad.*, stateless.*, javax.naming.*, java.util.*, java.math.BigDecimal"%>
<%!
private Libro2BeanRemote librocat=null;
String S1;
Collection list;

public void jspInit() {
try
{
InitialContext context = new InitialContext();
librocat=(Libro2BeanRemote) context.lookup (Libro2BeanRemote.class.getName());
System.out.println("cargando el catalogo bean ="+ librocat);
}

catch ( Exception e )
{
System.out.println("error"+e );
}
}

public void jspDestroy()
{
librocat = null;
}
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script>
            function confirmar(){
                if(confirm("Esta seguro de eliminar?")==true){
                    document.forms['formulario'].submit();
                }
            }
        </script>
        <title>buscar libro</title>
    </head>
    <body>
<%
try{
S1=request.getParameter("id");
        if(S1 != null)
           {
            Integer id =new Integer (S1);
            Libro elemento = librocat.findLibro(id);
            if(elemento!=null){
%>
Datos del libro
<br>

<p>ID:<b><%=elemento.getId()%> </b>  </p>
<p>Titulo :<b><%=elemento.getTitulo()%> </b>  </p>
<p>Autor:<b><%=elemento.getAutor()%> </b>  </p>
<p>Precio:<b><%=elemento.getPrecio()%> </b>  </p>
<br>
<p>
Eliminar?
<form method="POST" id="formulario" action="confirmaeliminacion.jsp">
    <input type="hidden" value="<%=S1%>" name="id">
    <input type="button" value="ELIMINAR" onclick="confirmar()">
</form>
<%
}else{
                %>
<p>El id <%=S1%> proporcionado no existe</p>
<%
            }
}
}
catch (Exception e )
{
    System.out.println("entra al catch ");        
e.printStackTrace ();
}

%>

Click <a href="index.html"> para regresar </a><br></p>

    </body>
</html>
